<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'complete-wordpress-developer-course-plugins-themes' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'isylzjkoD3v' );

/** Database hostname */
define( 'DB_HOST', 'db' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'tD[[hL?QL[<ci 0z(X4tkdgtpiV$>XxJ<Zt*&W-S:n08OBjAe)a@3Iua`qoMS61W' );
define( 'SECURE_AUTH_KEY',  'GX;Qy9DIuWdl!:30lhGWC{F7?X1M@PI&tm@j9%eDRvFd a8|r-Y0{a(q(_WLFVgW' );
define( 'LOGGED_IN_KEY',    '7`W,spw#SrFk#H;XNW%?RYWa@[6:&v%W&PN&#%BmxPDE.Z(j*:|$AqAujWaCK3}P' );
define( 'NONCE_KEY',        'tr#XSR G1d3u:*c.D8Y9Q:|CqfacQlmCFeH8DrT(jc]Z?;.rn#ppr$jz-(MisMY2' );
define( 'AUTH_SALT',        '<l)COErdOHF$QveHF?$A1fJ0~0R%T<wKahb!wJ{475I,X1|BdW:?zT#D8#<|KRha' );
define( 'SECURE_AUTH_SALT', 'JqZ<9W7n_ac1$pCKyg}rYi33hNqlAO|IvYT^C?2F?f%q,zS-7fj&hs(OyPM$8ENl' );
define( 'LOGGED_IN_SALT',   'LSff e*x}_j.b_x;>*ExGe/`6zd:{<#4m++G(DKLk[R=X77Ljq84/k^*/ciJYuT{' );
define( 'NONCE_SALT',       '@46eG4SLU,(] _;`vM<oKXnzV<^ycZEYx[-Y$Ki&GaR04w+y@5?#EzC!5a#27vml' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
